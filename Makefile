.PHONY: all clean realclean

VERSION ?= 2.0.1
XPI := linkalert-$(VERSION).xpi
SRCS := $(shell find icons/) $(shell find _locales/) $(shell find src/)
BINS := manifest.json

all: $(BINS) $(XPI)

%.json: %.json.in
	@echo "PRE " $@
	@cpp -P -C -nostdinc -DVERSION=\"$(VERSION)\" -o $@ $<

$(XPI): $(SRCS) $(BINS)
	@echo "ZIP " $@
	@zip $@ $^ > /dev/null

clean:
	@echo "RM  " $(XPI)
	@rm -f $(XPI)
	@echo "RM  " $(BINS)
	@rm -f $(BINS)

realclean: clean
	@echo "RM   *.xpi"
	@rm -f *.xpi

