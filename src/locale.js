/**
 * @file locale.js
 * Functions for localizing pages.
 */

/**
 * Scans the page for "locale" name and adds localized strings.
 */
function localize(){
	var list = document.getElementsByClassName("locale");
	for (var i = 0; i < list.length; i++) {
		var id = list[i].attributes["name"].value.split("-")[1];
		try{
			list[i].innerHTML = browser.i18n.getMessage(id);
		}catch(e){}
	}
}

document.addEventListener('DOMContentLoaded', localize);

