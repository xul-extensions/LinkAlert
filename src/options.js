/**
 * @file options.js
 * Functions for handling the options page.
 */

var currentOptions = {};

/**
 * Saves options as they are changed.
 */
function onChange(e){
	var name = e.target.id.split("-");
	var type = e.target.type;
	var value = false;
	if(type === "checkbox"){
		value = e.target.checked;
	}else if(type === "number"){
		value = parseInt(e.target.value);
	}else if(type !== "select-one"){
		value = e.target.value;
	}
	if(name[0] !== "advanced"){
		currentOptions[name[0]][name[1]] = value;
		chrome.storage.local.set(currentOptions);
	}
}

/**
 * Reorders the selected element of a select field.
 * @param id The id of the select field.
 * @param updown true to move selected element up, false to move element down.
 */
function reorderSelect(id, updown){
	var list = document.getElementById(id);
	var i = list.selectedIndex;
	var selectedChild = list.options[i];
	if(updown === true && i > 0){
		selectedChild = list.removeChild(selectedChild);
		list.insertBefore(selectedChild, list.options[i-1]);
	}else if(updown === false && i < list.options.length-1){
		var placeChild = selectedChild.nextSibling.nextSibling;
		selectedChild = list.removeChild(selectedChild);
		list.insertBefore(selectedChild, list.options[i+1]);
	}
	saveSelectOrder(id);
}

/**
 * onClick handler for up button.
 */
function onUpClick(){
	reorderSelect("display-priority", true);
}

/**
 * onClick handler for down button.
 */
function onDownClick(){
	reorderSelect("display-priority", false);
}

/**
 * onClick handler for path add button.
 * @param sub Name of the group of options to update.
 */
function onPathAdd(sub){
	var path = document.getElementById(sub+"-path-entry").value;
	var child = document.createElement("option");
	child.text = path;
	currentOptions["advanced"][sub+"-paths"][path] = new Array();
	var list = document.getElementById("advanced-"+sub+"-paths");
	list.add(child);
	list.value = path;
	onPathChange(sub);
	chrome.storage.local.set(currentOptions);
}

/**
 * onClick handler for the remove path button.
 * @param sub Name of the group of options to update.
 */
function onPathRem(sub){
	var list = document.getElementById("advanced-"+sub+"-paths");
	var path = list.value;
	list.remove(list.selectedIndex);
	delete currentOptions["advanced"][sub+"-paths"][path];
	chrome.storage.local.set(currentOptions);
	// Remove everything from the other box
	list = document.getElementById(sub+"-list");
	while(list.firstChild) {
		list.removeChild(list.firstChild);
	}
}

/**
 * onClick handler for the remove ext and proto buttons.
 * @param sub Name of the group of options to update.
 */
function onSpecRem(sub){
	var list = document.getElementById(sub+"-list");
	var spec = list.value;
	list.remove(list.selectedIndex);
	var path = document.getElementById("advanced-"+sub+"-paths").value;
	var array = currentOptions["advanced"][sub+"-paths"][path];
	var index = array.indexOf(spec);
	if(index > -1){
		array.splice(index, 1);
	}
	chrome.storage.local.set(currentOptions);
}

/**
 * onChange handler for file path lists.
 * @param sub Name of the group of options to update.
 */
function onPathChange(sub){
	var path = document.getElementById("advanced-"+sub+"-paths").value;
	var exts = currentOptions["advanced"][sub+"-paths"][path];
	var list = document.getElementById(sub+"-list");
	// Remove all children
	while(list.firstChild) {
		list.removeChild(list.firstChild);
	}
	// Add new children
	for(var i in exts){
		var option = document.createElement("option");
		option.text = exts[i];
		option.value = exts[i];
		list.appendChild(option);
	}
}

/**
 * onClick handler for extension add button.
 */
function onExtAdd(){
	var ext = document.getElementById("ext-entry").value;
	if(ext[0] !== '.'){
		ext = "." + ext;
	}
	var child = document.createElement("option");
	child.text = ext;
	var path = document.getElementById("advanced-ext-paths").value;
	if(!currentOptions["advanced"]["ext-paths"].hasOwnProperty(path)){
		currentOptions["advanced"]["ext-paths"][path] = new Array();
	}
	currentOptions["advanced"]["ext-paths"][path].push(ext);
	var list = document.getElementById("ext-list")
	list.add(child);
	list.value = ext;
	chrome.storage.local.set(currentOptions);
}

/**
 * onClick handler for protocol add button.
 */
function onProtoAdd(){
	var ext = document.getElementById("proto-entry").value;
	var child = document.createElement("option");
	child.text = ext;
	var path = document.getElementById("advanced-proto-paths").value;
	if(!currentOptions["advanced"]["proto-paths"].hasOwnProperty(path)){
		currentOptions["advanced"]["proto-paths"][path] = new Array();
	}
	currentOptions["advanced"]["proto-paths"][path].push(ext);
	var list = document.getElementById("proto-list")
	list.add(child);
	list.value = ext;
	chrome.storage.local.set(currentOptions);
}

/**
 * Save order of select options.
 * @param id The id of the select field.
 */
function saveSelectOrder(id){
	var name = id.split("-");
	var list = document.getElementById(id);
	for(var i = 0; i < list.options.length; i++){
		currentOptions[name[0]][name[1]][i] = list.options[i].value;
	}
	chrome.storage.local.set(currentOptions);
}

/**
 * Finds and returns an option in a select field.
 * @param list The select node to search.
 * @param opt Value of the option to find.
 * @return The option node or null if nothing was found.
 */
function findOption(list, opt){
	for(var i = 0; i < list.options.length; i++){
		if(list.options[i].value == opt){
			return list.options[i];
		}
	}
	return null;
}

/**
 * Restores the order of options in a select field.
 * @param id The id of the select field.
 */
function restoreSelectOrder(id){
	var name = id.split("-");
	var opts = currentOptions[name[0]][name[1]];
	var list = document.getElementById(id);
	for(var i = 0; i < opts.length; i++){
		var child = findOption(list, opts[i]);
		if(child != null){
			child = list.removeChild(child);
			list.insertBefore(child, list.options[i]);
		}
	}
}

/**
 * Restores all options fields on the page.
 */
function restoreOptions() {
	chrome.storage.local.get(null, (res) => {
		// Load default options
		currentOptions = defaultOptions;
		// Overlay with stored options
		for(var key1 in res){
			if(res.hasOwnProperty(key1)){
				for(var key2 in res[key1]){
					if(res[key1].hasOwnProperty(key2)){
						currentOptions[key1][key2] = res[key1][key2];
					}
				}
			}
		}

		// Set fields
		for(var key1 in currentOptions){
			if(currentOptions.hasOwnProperty(key1)){
				for(var key2 in currentOptions[key1]){
					if(currentOptions[key1].hasOwnProperty(key2)){
						var field = document.getElementById(key1+"-"+key2);
						if(field != null){
							var type = field.type;
							if(type === "checkbox"){
								field.checked = currentOptions[key1][key2];
							}else if(key1 === "advanced" && type === "select-one"){
								for(var path in currentOptions[key1][key2]){
									if(currentOptions[key1][key2].hasOwnProperty(path)){
										var option = document.createElement("option");
										option.text = path;
										option.value = path;
										field.appendChild(option);
									}
								}
							}else{
								field.value = currentOptions[key1][key2];
							}
						}
					}
				}
			}
		}
		restoreSelectOrder("display-priority");

		// Add onChange
		var fields = document.getElementsByTagName("INPUT");
		for (var i = 0; i < fields.length; i++) {
			if(fields[i].type !== "text"){
				fields[i].addEventListener("change", onChange);
			}
		}
		// For some reason the onclick attribute doesn't work, so add onClick here
		document.getElementById("up-button").addEventListener("click", onUpClick);
		document.getElementById("down-button").addEventListener("click", onDownClick);
		document.getElementById("ext-path-add-button").addEventListener("click", function() { onPathAdd("ext"); });
		document.getElementById("proto-path-add-button").addEventListener("click", function() { onPathAdd("proto"); });
		document.getElementById("ext-path-rem-button").addEventListener("click", function() { onPathRem("ext"); });
		document.getElementById("proto-path-rem-button").addEventListener("click", function() { onPathRem("proto"); });
		document.getElementById("ext-add-button").addEventListener("click", onExtAdd);
		document.getElementById("proto-add-button").addEventListener("click", onProtoAdd);
		document.getElementById("ext-rem-button").addEventListener("click", function() { onSpecRem("ext"); });
		document.getElementById("proto-rem-button").addEventListener("click", function() { onSpecRem("proto"); });
		document.getElementById("advanced-ext-paths").addEventListener("change", function() { onPathChange("ext"); });
		document.getElementById("advanced-proto-paths").addEventListener("change", function() { onPathChange("proto"); });
	});
}

document.addEventListener('DOMContentLoaded', restoreOptions);

